package za.co.bbd.blackswanassessment.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import za.co.bbd.blackswanassessment.dto.UserDTO;
import za.co.bbd.blackswanassessment.error.ResourceNotFoundException;
import za.co.bbd.blackswanassessment.mapper.UserMapper;
import za.co.bbd.blackswanassessment.model.User;
import za.co.bbd.blackswanassessment.repository.UserRepository;
import za.co.bbd.blackswanassessment.service.impl.UserServiceImpl;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    public static final String USERNAME = "username";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";
    public static final long USER_ID = 1L;

    private UserService userService;

    @Mock
    UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        userService = new UserServiceImpl(UserMapper.INSTANCE,userRepository);
    }

    @Test
    public void getAllTasksByUserId(){

        //given
        List<User> users = Arrays.asList(new User(),new User(),new User());

        when(userRepository.findAll()).thenReturn(users);

        //when
        List<UserDTO> userDTOS = userService.getAllUsers();

        //then
        assertEquals(3,userDTOS.size());

    }

    @Test(expected=ResourceNotFoundException.class)
    public void getUserNotFound(){

        //when
        UserDTO userDTO = userService.getUserById(USER_ID);

        //then
        then(userRepository).should(times(1)).findOne(anyLong());

    }


    @Test
    public void getUserById(){

        //given
        User user = getUserDate();

        when(userRepository.findOne(USER_ID)).thenReturn(user);

        //when
        UserDTO userDTO = userService.getUserById(USER_ID);

        //then
        then(userRepository).should(times(1)).findOne(anyLong());
        assertThat(userDTO.getUserId(), is(equalTo(USER_ID)));
        assertEquals(USERNAME,userDTO.getUsername());
        assertEquals(FIRSTNAME,userDTO.getFirstName());
        assertEquals(LASTNAME,userDTO.getLastName());

    }

    @Test
    public void  createNewUser(){

        //given
        UserDTO newUser = new UserDTO();
        newUser.setUsername("fdeysel");

        User savedUser = new User();
        savedUser.setUsername(newUser.getUsername());

        when(userRepository.save(any(User.class))).thenReturn(savedUser);

        //when
        UserDTO resultUser = userService.createNewUser(newUser);

        //then
        assertEquals(resultUser.getUsername(),newUser.getUsername());

    }

    @Test
    public void  updateUserById(){

        //given
        UserDTO newUser = new UserDTO();
        newUser.setUsername("fdeysel");

        User savedUser = new User();
        savedUser.setUserId(USER_ID);
        savedUser.setUsername(newUser.getUsername());

        when(userRepository.save(any(User.class))).thenReturn(savedUser);
        when(userRepository.findOne(anyLong())).thenReturn(savedUser);

        //when
        UserDTO resultUser = userService.updateUserById(USER_ID,newUser);

        //then
        assertEquals(resultUser.getUsername(),savedUser.getUsername());
        assertThat(resultUser.getUserId(), is(equalTo(USER_ID)));
    }


    private User getUserDate(){
        User user = new User();
        user.setUserId(USER_ID);
        user.setUsername(USERNAME);
        user.setFirstName(FIRSTNAME);
        user.setLastName(LASTNAME);
        return user;
    }

}