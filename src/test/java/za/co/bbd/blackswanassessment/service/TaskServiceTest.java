package za.co.bbd.blackswanassessment.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import za.co.bbd.blackswanassessment.dto.TaskDTO;
import za.co.bbd.blackswanassessment.error.ResourceNotFoundException;
import za.co.bbd.blackswanassessment.mapper.TaskMapper;
import za.co.bbd.blackswanassessment.model.Task;
import za.co.bbd.blackswanassessment.model.User;
import za.co.bbd.blackswanassessment.repository.TaskRepository;
import za.co.bbd.blackswanassessment.repository.UserRepository;
import za.co.bbd.blackswanassessment.service.impl.TaskServiceImpl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

public class TaskServiceTest {

    public static final String TASK_NAME = "TaskName";
    public static final String DESCRIPTION = "Description";
    private TaskService taskService;

    private TaskMapper taskMapper = TaskMapper.INSTANCE;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private UserRepository userRepository;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        taskService = new TaskServiceImpl(taskMapper,taskRepository,userRepository);
    }

    @Test
    public void getAllTasksByUserId(){

        List<Task> tasks = Arrays.asList(new Task(),new Task());

        when(userRepository.findOne(anyLong())).thenReturn(new User());
        when(taskRepository.findByUser(any(User.class))).thenReturn(tasks);

        List<TaskDTO> taskDTOS = taskService.getAllTasksByUserId(1L);

        assertEquals(2,taskDTOS.size());

    }

    @Test
    public void getTaskById(){

        Task task = new Task();
        task.setName(TASK_NAME);
        task.setDescription(DESCRIPTION);
        Date now = new Date();
        task.setDateTime(now);

        when(userRepository.findOne(anyLong())).thenReturn(new User());
        when(taskRepository.findByUserAndTaskId(any(User.class),anyLong())).thenReturn(task);

        TaskDTO taskDTO = taskService.getTaskById(1L, 1L);

        then(taskRepository).should(times(1)).findByUserAndTaskId(any(User.class),anyLong());
        assertEquals(TASK_NAME,taskDTO.getName());
        assertEquals(DESCRIPTION,taskDTO.getDescription());
        assertEquals(now,taskDTO.getDateTime());

    }

    @Test
    public void createNewTask(){

        TaskDTO newTaskDTO =  new TaskDTO();
        newTaskDTO.setName(TASK_NAME);
        newTaskDTO.setDescription(DESCRIPTION);

        Task savedTask = new Task();
        savedTask.setName(newTaskDTO.getName());
        savedTask.setDescription(newTaskDTO.getDescription());

        when(userRepository.findOne(anyLong())).thenReturn(new User());
        when(taskRepository.save(any(Task.class))).thenReturn(savedTask);


        TaskDTO result = taskService.createNewTask(1L, newTaskDTO);


        assertEquals(result.getName(),newTaskDTO.getName());

    }

    @Test
    public void updateTask(){

        TaskDTO newTaskDTO =  new TaskDTO();
        newTaskDTO.setName(TASK_NAME);
        newTaskDTO.setDescription(DESCRIPTION);

        Task savedTask = new Task();
        savedTask.setName(newTaskDTO.getName());
        savedTask.setDescription(newTaskDTO.getDescription());

        when(taskRepository.findByUserAndTaskId(any(User.class),anyLong())).thenReturn(new Task());
        when(userRepository.findOne(anyLong())).thenReturn(new User());
        when(taskRepository.save(any(Task.class))).thenReturn(savedTask);

        TaskDTO result = taskService.updateTask(1L, 1L,newTaskDTO);

        assertEquals(result.getName(),newTaskDTO.getName());
        assertEquals(result.getDescription(),newTaskDTO.getDescription());

    }

    @Test
    public void deleteTaskById(){

        when(userRepository.findOne(anyLong())).thenReturn(new User());
        when(taskRepository.findByUserAndTaskId(any(User.class),anyLong())).thenReturn(new Task());

        taskService.deleteTaskById(1L, 1L);

        verify(taskRepository,times(1)).delete(any(Task.class));

    }

    @Test(expected=ResourceNotFoundException.class)
    public void taskNotFound(){

        Task task = new Task();
        task.setName(TASK_NAME);
        task.setDescription(DESCRIPTION);
        Date now = new Date();
        task.setDateTime(now);

        when(userRepository.findOne(anyLong())).thenReturn(new User());

        TaskDTO taskDTO = taskService.getTaskById(1L, 1L);

        then(taskRepository).should(times(1)).findByUserAndTaskId(any(User.class),anyLong());

    }

    @Test
    public void updateStatusDateTimePassed(){

        List<Task> tasks = Arrays.asList(new Task(),new Task());

        when(userRepository.findOne(anyLong())).thenReturn(new User());
        when(taskRepository.findByStatusAndDateTimeLessThan(anyString(),any(Date.class))).thenReturn(tasks);

        List<TaskDTO> taskDTOS = taskService.updateStatusDateTimePassed();

        assertEquals(2,taskDTOS.size());


    }


}