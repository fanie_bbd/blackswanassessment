package za.co.bbd.blackswanassessment.mapper;

import org.junit.Before;
import org.junit.Test;
import za.co.bbd.blackswanassessment.dto.TaskDTO;
import za.co.bbd.blackswanassessment.model.Task;

import javax.xml.crypto.Data;

import java.util.Date;

import static org.junit.Assert.*;

public class TaskMapperTest {


    TaskMapper taskMapper = TaskMapper.INSTANCE;

    Task task;
    TaskDTO taskDTO;

    Date date = new Date();

    @Before
    public void setUp() throws Exception {

        task = new Task();
        task.setName("name");
        task.setDescription("des");
        task.setDateTime(date);

        taskDTO = new TaskDTO();
        taskDTO.setName("name");
        taskDTO.setDescription("des");
        taskDTO.setDateTime(date);

    }

    @Test
    public void taskToTaskDTO() {

        TaskDTO taskDTOnew = taskMapper.taskToTaskDTO(task);

        assertEquals(taskDTOnew.getName(),task.getName());
        assertEquals(taskDTOnew.getDescription(),task.getDescription());
        assertEquals(taskDTOnew.getDateTime(),task.getDateTime());


    }

    @Test
    public void taskDTOToTask() {

        Task taskNew = taskMapper.taskDTOToTask(taskDTO);

        assertEquals(taskNew.getName(),task.getName());
        assertEquals(taskNew.getDescription(),task.getDescription());
        assertEquals(taskNew.getDateTime(),task.getDateTime());


    }



}