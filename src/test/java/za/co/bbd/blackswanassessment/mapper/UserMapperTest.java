package za.co.bbd.blackswanassessment.mapper;

import org.junit.Before;
import org.junit.Test;
import za.co.bbd.blackswanassessment.dto.UserDTO;
import za.co.bbd.blackswanassessment.model.User;

import static org.junit.Assert.*;

public class UserMapperTest {

    UserMapper userMapper = UserMapper.INSTANCE;

    User user;
    UserDTO userDTO;

    @Before
    public void setUp() throws Exception {

        user = new User();
        user.setUsername("username1");
        user.setFirstName("firstname1");
        user.setLastName("lastname1");

        userDTO = new UserDTO();
        userDTO.setUsername("username1");
        userDTO.setFirstName("firstname1");
        userDTO.setLastName("lastname1");
    }

    @Test
    public void userToUserDTO() {

        UserDTO userDTOnew = userMapper.userToUserDTO(user);

        assertEquals(user.getUsername(),userDTOnew.getUsername());
        assertEquals(user.getFirstName(),userDTOnew.getFirstName());
        assertEquals(user.getLastName(),userDTOnew.getLastName());

    }

    @Test
    public void userDTOToUser() {

        User userNew = userMapper.userDTOToUser(userDTO);

        assertEquals(userNew.getUsername(),userDTO.getUsername());
        assertEquals(userNew.getFirstName(),userDTO.getFirstName());
        assertEquals(userNew.getLastName(),userDTO.getLastName());

    }


}