package za.co.bbd.blackswanassessment.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import za.co.bbd.blackswanassessment.dto.UserDTO;
import za.co.bbd.blackswanassessment.service.UserService;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static za.co.bbd.blackswanassessment.controller.AbstractRestControllerTest.asJsonString;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {UserController.class})
public class UserControllerTest {

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;


    UserDTO userDTO1;
    UserDTO userDTO2;


    @Before
    public void setUp() throws Exception {

        userDTO1 = new UserDTO();
        userDTO1.setUsername("username1");
        userDTO1.setFirstName("firstname1");
        userDTO1.setLastName("lastname1");

        userDTO2 = new UserDTO();
        userDTO2.setUsername("username2");
        userDTO2.setFirstName("firstname2");
        userDTO2.setLastName("lastname2");

    }

    @Test
    public void getListOfUsers() throws Exception {

        List<UserDTO> userDTOS = Arrays.asList(userDTO1, userDTO2);

        when(userService.getAllUsers()).thenReturn(userDTOS);

        mockMvc.perform(get(UserController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.users", hasSize(2)));

    }

    @Test
    public void getUserById() throws Exception {

        given(userService.getUserById(anyLong())).willReturn(userDTO1);

        mockMvc.perform(get(UserController.BASE_URL + "/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", equalTo(userDTO1.getUsername())));

    }

    @Test
    public void createNewUser() throws Exception {

        given(userService.createNewUser(userDTO1)).willReturn(userDTO1);

        mockMvc.perform(post(UserController.BASE_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDTO1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.username", equalTo(userDTO1.getUsername())));


    }

    @Test
    public void updateUser() throws Exception {

        given(userService.updateUserById(1L,userDTO1)).willReturn(userDTO1);

        mockMvc.perform(put(UserController.BASE_URL + "/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(userDTO1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", equalTo(userDTO1.getUsername())));

    }
}