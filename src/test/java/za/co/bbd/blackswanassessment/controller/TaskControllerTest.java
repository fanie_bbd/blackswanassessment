package za.co.bbd.blackswanassessment.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import za.co.bbd.blackswanassessment.dto.TaskDTO;
import za.co.bbd.blackswanassessment.service.TaskService;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static za.co.bbd.blackswanassessment.controller.AbstractRestControllerTest.asJsonString;


@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {TaskController.class})
public class TaskControllerTest {

    @MockBean
    private TaskService taskService;

    @Autowired
    private MockMvc mockMvc;

    TaskDTO taskDTO1;
    TaskDTO taskDTO2;

    @Before
    public void setUp() throws Exception {

        taskDTO1 = new TaskDTO();
        taskDTO1.setName("name");
        taskDTO2 = new TaskDTO();


    }

    @Test
    public void getAllTasksByUser() throws Exception {

        List<TaskDTO> taskDTOList = Arrays.asList(taskDTO1, taskDTO2);

        when(taskService.getAllTasksByUserId(anyLong())).thenReturn(taskDTOList);

        mockMvc.perform(get(UserController.BASE_URL + "/1/task")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.tasks", hasSize(2)));

    }

    @Test
    public void getTaskById() throws Exception {

        given(taskService.getTaskById(anyLong(),anyLong())).willReturn(taskDTO1);

        mockMvc.perform(get(UserController.BASE_URL + "/1/task/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo(taskDTO1.getName())));



    }

    @Test
    public void createNewTask() throws Exception {

        given(taskService.createNewTask(1L,taskDTO1)).willReturn(taskDTO1);

        mockMvc.perform(post(UserController.BASE_URL + "/1/task")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(taskDTO1)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", equalTo(taskDTO1.getName())));

    }

    @Test
    public void updateTask() throws Exception {

        given(taskService.updateTask(1L,1L,taskDTO1)).willReturn(taskDTO1);

        mockMvc.perform(put(UserController.BASE_URL + "/1/task/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(taskDTO1)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", equalTo(taskDTO1.getName())));



    }

    @Test
    public void deleteTask() throws Exception {

        mockMvc.perform(delete(UserController.BASE_URL + "/1/task/1"))
                .andExpect(status().isOk());

        then(taskService).should().deleteTaskById(anyLong(),anyLong());
    }
}