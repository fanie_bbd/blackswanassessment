package za.co.bbd.blackswanassessment.service;

import za.co.bbd.blackswanassessment.dto.TaskDTO;

import java.util.List;

/**
 * Task service interface
 */
public interface TaskService {

    List<TaskDTO> getAllTasksByUserId(Long userId);

    TaskDTO getTaskById(Long userId, Long id);

    TaskDTO createNewTask(Long user_id, TaskDTO taskDTO);

    TaskDTO updateTask(Long userId, Long id, TaskDTO taskDTO);

    void deleteTaskById(Long userId, long taskID);

    List<TaskDTO> updateStatusDateTimePassed();


}
