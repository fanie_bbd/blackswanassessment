package za.co.bbd.blackswanassessment.service.impl;

import org.springframework.stereotype.Service;
import za.co.bbd.blackswanassessment.dto.UserDTO;
import za.co.bbd.blackswanassessment.error.ResourceNotFoundException;
import za.co.bbd.blackswanassessment.mapper.UserMapper;
import za.co.bbd.blackswanassessment.model.User;
import za.co.bbd.blackswanassessment.repository.UserRepository;
import za.co.bbd.blackswanassessment.service.UserService;

import java.util.List;
import java.util.stream.Collectors;


/**
 * User service implementation
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;

    public UserServiceImpl(UserMapper userMapper, UserRepository userRepository) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
    }

    @Override
    public List<UserDTO> getAllUsers() {
        return userRepository
                .findAll()
                .stream()
                .map(userMapper::userToUserDTO)
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO getUserById(Long id) {
        return userMapper.userToUserDTO(getUser(id));
    }

    @Override
    public UserDTO createNewUser(UserDTO userDTO) {

        return saveAndReturnDTO(userMapper.userDTOToUser(userDTO));
    }

    @Override
    public UserDTO updateUserById(Long id, UserDTO userDTO) {

        User user = getUser(id);

        if (userDTO.getUsername() == null){
            userDTO.setUsername(user.getUsername());
        }

        if (userDTO.getFirstName() == null){
            userDTO.setFirstName(user.getFirstName());
        }

        if (userDTO.getLastName() == null){
            userDTO.setLastName(user.getLastName());
        }

        userDTO.setUserId(id);
        return saveAndReturnDTO(userMapper.userDTOToUser(userDTO));
    }



    private UserDTO saveAndReturnDTO(User user){

        User saveUser = userRepository.save(user);

        UserDTO userDTO = userMapper.userToUserDTO(saveUser);

        return userDTO;
    }

    private User getUser(Long userId){

        User user = userRepository.findOne(userId);

        if (user == null) {
            throw new ResourceNotFoundException("user id " + userId + " not found");
        }

        return user;
    }



}
