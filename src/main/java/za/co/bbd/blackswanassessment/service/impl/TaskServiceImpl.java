package za.co.bbd.blackswanassessment.service.impl;

import org.springframework.stereotype.Service;
import za.co.bbd.blackswanassessment.dto.TaskDTO;
import za.co.bbd.blackswanassessment.error.ResourceNotFoundException;
import za.co.bbd.blackswanassessment.mapper.TaskMapper;
import za.co.bbd.blackswanassessment.model.Task;
import za.co.bbd.blackswanassessment.model.User;
import za.co.bbd.blackswanassessment.repository.TaskRepository;
import za.co.bbd.blackswanassessment.repository.UserRepository;
import za.co.bbd.blackswanassessment.service.TaskService;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Task service implementation
 */
@Service
public class TaskServiceImpl implements TaskService {

    private final TaskMapper taskMapper;
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;

    public TaskServiceImpl(TaskMapper taskMapper, TaskRepository taskRepository, UserRepository userRepository) {
        this.taskMapper = taskMapper;
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<TaskDTO> getAllTasksByUserId(Long userId) {

        User user = getUser(userId);

        return taskRepository.findByUser(user)
                .stream()
                .map(taskMapper::taskToTaskDTO)
                .collect(Collectors.toList());
    }

    @Override
    public TaskDTO getTaskById(Long userId, Long id) {

        TaskDTO taskDTO  = taskMapper.taskToTaskDTO(getTask(userId,id));

        return taskDTO;
    }

    @Override
    public TaskDTO createNewTask(Long userId, TaskDTO taskDTO) {

        Task task = taskMapper.taskDTOToTask(taskDTO);
        User user = getUser(userId);
        task.setUser(user);

        return saveAndReturnDTO(task);
    }

    @Override
    public TaskDTO updateTask(Long userId, Long id, TaskDTO taskDTO) {

        Task validTask = getTask(userId,id);
        Task task = taskMapper.taskDTOToTask(taskDTO);
        task.setUser(validTask.getUser());
        task.setTaskId(validTask.getTaskId());

        if (task.getName() == null){
            task.setName(validTask.getName());
        }

        if (task.getDescription() == null){
            task.setDescription(validTask.getDescription());
        }

        if (task.getStatus()  == null){
            task.setStatus(validTask.getStatus());
        }

        if (task.getDateTime() == null){
            task.setDateTime(validTask.getDateTime());
        }

        return saveAndReturnDTO(task);
    }

    @Override
    public void deleteTaskById(Long userId, long taskID) {

        Task validTask = getTask(userId,taskID);

        taskRepository.delete(validTask);
    }

    @Override
    public List<TaskDTO> updateStatusDateTimePassed() {

        List<Task> taskList = taskRepository.findByStatusAndDateTimeLessThan("pending",new Date());

        for ( Task task : taskList){
            task.setStatus("done");
        }

        taskRepository.save(taskList);

        List<TaskDTO> taskDTOList = taskList
                .stream()
                .map(taskMapper::taskToTaskDTO)
                .collect(Collectors.toList());

        return taskDTOList;
    }

    private TaskDTO saveAndReturnDTO(Task task){

        Task saveTask = taskRepository.save(task);
        TaskDTO taskDTO = taskMapper.taskToTaskDTO(saveTask);

        return taskDTO;
    }

    private User getUser(Long userId){

        User user = userRepository.findOne(userId);

        if (user == null) {
            throw new ResourceNotFoundException("user id " + userId + " not found");
        }

        return user;
    }

    private Task getTask(Long used_id,Long task_id){

        Task task =  taskRepository.findByUserAndTaskId(getUser(used_id),task_id);
        if (task == null){
            throw new ResourceNotFoundException("Task for user id " + used_id + " and task id " + task_id + " not found");
        }
        return task;
    }



}
