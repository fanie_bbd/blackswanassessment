package za.co.bbd.blackswanassessment.service;

import za.co.bbd.blackswanassessment.dto.UserDTO;

import java.util.List;


/**
 * User service interface
 */
public interface UserService {


    List<UserDTO> getAllUsers();

    UserDTO getUserById(Long id);

    UserDTO createNewUser(UserDTO userDTO);

    UserDTO updateUserById(Long id,UserDTO userDTO);



}
