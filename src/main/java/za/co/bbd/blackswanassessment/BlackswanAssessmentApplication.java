package za.co.bbd.blackswanassessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class BlackswanAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlackswanAssessmentApplication.class, args);
	}
}
