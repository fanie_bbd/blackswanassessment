package za.co.bbd.blackswanassessment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.bbd.blackswanassessment.model.Task;
import za.co.bbd.blackswanassessment.model.User;

import java.util.Date;
import java.util.List;

/**
 * JPA specific extension of Repository.
 */
public interface TaskRepository extends JpaRepository<Task,Long> {

    List<Task> findByUser(User user);

    Task findByUserAndTaskId(User user,Long taskId);

    List<Task> findByStatusAndDateTimeLessThan(String status,Date date);

}
