package za.co.bbd.blackswanassessment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.bbd.blackswanassessment.model.User;


/**
 * JPA specific extension of Repository.
 */
public interface UserRepository extends JpaRepository<User,Long> {

}
