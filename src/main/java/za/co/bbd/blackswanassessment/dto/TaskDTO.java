package za.co.bbd.blackswanassessment.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Size;
import java.util.Date;


/**
 * A data transfer object for task
 */
@Data
public class TaskDTO {

    @JsonProperty("task_id")
    private Long taskId;

    @Size(min=2, message="Task Name should have atleast 2 characters")
    private String name;

    @Size(min=2, message="Description should have atleast 2 characters")
    private String description;

    @JsonProperty("date_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dateTime;

    @Size(min=1, message="Status should have atleast 1 characters")
    private String status;

}
