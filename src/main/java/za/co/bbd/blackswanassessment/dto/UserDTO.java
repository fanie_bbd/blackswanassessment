package za.co.bbd.blackswanassessment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.Size;

/**
 * A data transfer object for user
 */

@Data
public class UserDTO {

    @JsonProperty("user_id")
    private Long userId;

    @Size(min=2, message="Username should have atleast 2 characters")
    private String username;

    @Size(min=2, message="first_name should have atleast 2 characters")
    @JsonProperty("first_name")
    private String firstName;

    @Size(min=2, message="last_name should have atleast 2 characters")
    @JsonProperty("last_name")
    private String lastName;

}
