package za.co.bbd.blackswanassessment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;


/**
 * A data transfer object for task list
 */
@Data
@AllArgsConstructor
public class TaskListDTO {

    List<TaskDTO> tasks;

}
