package za.co.bbd.blackswanassessment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * A data transfer object for user list
 */
@Data
@AllArgsConstructor
public class UserListDTO {

    List<UserDTO> users;

}
