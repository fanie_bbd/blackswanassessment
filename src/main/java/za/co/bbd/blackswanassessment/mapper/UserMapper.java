package za.co.bbd.blackswanassessment.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import za.co.bbd.blackswanassessment.dto.UserDTO;
import za.co.bbd.blackswanassessment.model.User;


/**
 * MapStruct is a code generator that greatly simplifies the implementation of mappings between Java bean types based on a convention over configuration approach.
 */
@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDTO userToUserDTO(User user);

    User userDTOToUser(UserDTO userDTO);

}
