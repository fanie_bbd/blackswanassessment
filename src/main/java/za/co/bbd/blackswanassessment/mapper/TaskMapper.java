package za.co.bbd.blackswanassessment.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import za.co.bbd.blackswanassessment.dto.TaskDTO;
import za.co.bbd.blackswanassessment.model.Task;


/**
 * MapStruct is a code generator that greatly simplifies the implementation of mappings between Java bean types based on a convention over configuration approach.
 */
@Mapper
public interface TaskMapper {

    TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);

    TaskDTO taskToTaskDTO(Task task);

    Task taskDTOToTask(TaskDTO taskDTO);

}
