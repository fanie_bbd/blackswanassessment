package za.co.bbd.blackswanassessment.bootstrap;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import za.co.bbd.blackswanassessment.model.Task;
import za.co.bbd.blackswanassessment.model.User;
import za.co.bbd.blackswanassessment.repository.TaskRepository;
import za.co.bbd.blackswanassessment.repository.UserRepository;

import java.util.Date;

@Slf4j
@Profile("dev")
@Component
public class Bootstrap implements CommandLineRunner {


    private UserRepository userRepository;
    private TaskRepository taskRepository;

    public Bootstrap(UserRepository userRepository, TaskRepository taskRepository) {
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
    }

    /**
     * Setting up some demo data to work with
     *
     * @param args
     * @throws Exception
     */
    @Override
    public void run(String... args) throws Exception {
        log.info("****************** DEV PROFILE LOADED ***********************");
        log.info("Setting up some demo data to work with.");

        User u1 = new User();
        u1.setFirstName("Fanie");
        u1.setLastName("Deysel");
        u1.setUsername("fdeysel");

        userRepository.save(u1);

        Task t1 = new Task();
        t1.setName("My Task 1");
        t1.setDescription("Task 1 Description");

        Date now = new Date();

        t1.setDateTime(now);
        t1.setStatus("pending");
        t1.setUser(u1);

        taskRepository.save(t1);

    }
}
