package za.co.bbd.blackswanassessment.scheduling;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import za.co.bbd.blackswanassessment.dto.TaskDTO;
import za.co.bbd.blackswanassessment.service.TaskService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class ScheduledTasks {

    private TaskService taskService;

    public ScheduledTasks(TaskService taskService) {
        this.taskService = taskService;
    }

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    /**
     * Setup a scheduled job to check all tasks in the Database - those that have a status of "pending" and who date_time has
     * passed - print it to the console and update the task to "done".
     */
    @Scheduled(fixedRate = 1000 * 60)
    public void updateTasks() {

        log.info("************************** Start ScheduledTasks *******************************************");
        log.info("The time is now {}", dateFormat.format(new Date()));

        List<TaskDTO> updatedTasks = taskService.updateStatusDateTimePassed();

        if (updatedTasks.size() == 0){
            log.info("No Tasks to update");
        } else {
            for (TaskDTO taskDTO : updatedTasks){
                log.info("Tasks id {} updated.",taskDTO.getTaskId());
            }
        }
        log.info("************************** End ScheduledTasks *******************************************");

    }
}
