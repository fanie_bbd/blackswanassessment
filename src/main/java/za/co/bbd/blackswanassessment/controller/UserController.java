package za.co.bbd.blackswanassessment.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import za.co.bbd.blackswanassessment.dto.UserDTO;
import za.co.bbd.blackswanassessment.dto.UserListDTO;
import za.co.bbd.blackswanassessment.service.UserService;

import javax.validation.Valid;

/**
 * The application should be able to list/create/update/delete tasks for users via REST.
 */
@Api(tags = {"User"})
@RestController
@RequestMapping(UserController.BASE_URL)
public class UserController {

    public static final String BASE_URL = "/api/user";

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }


    @ApiOperation(value = "Get list of users", notes = "", response = UserListDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Returns list of all users", response = UserListDTO.class) })
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public UserListDTO getListOfUsers(){
        return new UserListDTO(userService.getAllUsers());
    }

    @ApiOperation(value = "Get user by id", notes = "", response = UserDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Returns user", response = UserDTO.class) })
    @GetMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public UserDTO getUserById(@PathVariable Long id){
        return userService.getUserById(id);
    }

    @ApiOperation(value = "Create new user", notes = "", response = UserDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Returns new user", response = UserDTO.class) })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO createNewUser(@Valid @RequestBody UserDTO userDTO){
        return userService.createNewUser(userDTO);
    }

    @ApiOperation(value = "Update user by id", notes = "", response = UserDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Returns updated user", response = UserDTO.class) })
    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public UserDTO updateUser(@PathVariable Long id, @Valid @RequestBody UserDTO userDTO){
        return userService.updateUserById(id, userDTO);
    }

}
