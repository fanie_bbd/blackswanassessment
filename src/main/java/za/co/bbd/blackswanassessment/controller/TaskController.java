package za.co.bbd.blackswanassessment.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import za.co.bbd.blackswanassessment.dto.TaskDTO;
import za.co.bbd.blackswanassessment.dto.TaskListDTO;
import za.co.bbd.blackswanassessment.service.TaskService;

import javax.validation.Valid;


/**
 * The application should be able to list/create/update users via REST.
 */
@Api(tags = {"Task"})
@RestController
@RequestMapping(UserController.BASE_URL)
public class TaskController {

    private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @ApiOperation(value = "Get all tasks of an user", notes = "", response = TaskListDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Returns the task list", response = TaskListDTO.class) })
    @GetMapping({"/{user_id}/task"})
    @ResponseStatus(HttpStatus.OK)
    public TaskListDTO getAllTasksByUser(@PathVariable Long user_id){
        return new TaskListDTO(taskService.getAllTasksByUserId(user_id));
    }

    @ApiOperation(value = "Get task by id of an user", notes = "", response = TaskDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Returns the task", response = TaskDTO.class) })
    @GetMapping({"/{user_id}/task/{task_id}"})
    @ResponseStatus(HttpStatus.OK)
    public TaskDTO getTaskById(@PathVariable Long user_id, @PathVariable Long task_id){
        return taskService.getTaskById(user_id,task_id);
    }

    @ApiOperation(value = "Create new task of an user", notes = "", response = TaskDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Returns the new task", response = TaskDTO.class) })
    @PostMapping({"/{user_id}/task"})
    @ResponseStatus(HttpStatus.CREATED)
    public TaskDTO createNewTask(@PathVariable Long user_id,@Valid @RequestBody TaskDTO taskDTO){
        return taskService.createNewTask(user_id,taskDTO);
    }

    @ApiOperation(value = "Update task of an user", notes = "", response = TaskDTO.class)
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Returns the updated task", response = TaskDTO.class) })
    @PutMapping({"/{user_id}/task/{task_id}"})
    @ResponseStatus(HttpStatus.OK)
    public TaskDTO updateTask(@PathVariable Long user_id,@PathVariable Long task_id,@Valid @RequestBody TaskDTO taskDTO){
        return taskService.updateTask(user_id,task_id,taskDTO);
    }


    @ApiOperation(value = "Delete task of an user", notes = "")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Delete the updated task") })
    @DeleteMapping({"/{user_id}/task/{task_id}"})
    @ResponseStatus(HttpStatus.OK)
    public void deleteTask(@PathVariable Long user_id,@PathVariable Long task_id){
        taskService.deleteTaskById(user_id,task_id);
    }

}
